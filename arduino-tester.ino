// Includes
#include <ModbusRtu.h>
 
// Defines
#define MOD_BUS_ID  5
#define MOD_BUS_BAUD 9600 
#define TX_EN_PIN   2
#define BUZZER_PIN  13
#define RF_RELAY_PIN    3
#define RES_RELAY_PIN   5
#define FAN1_RELAY_PIN  6
//#define FAN2_RELAY_PIN  5
#define FAN3_RELAY_PIN  11
//#define FAN4_RELAY_PIN  9
#define FAN5_RELAY_PIN  10
#define MOD_ID0_PIN     4
#define MOD_ID1_PIN     7
#define MOD_ID2_PIN     8
#define MOD_ID3_PIN     12
#define Micro_SW_PIN    7
#define VOLT_PIN        6
#define Current_PIN     0

// Variables
Modbus modbus_port;

// modbus_array = { Curret_High ,Curret_Low ,Volt_High ,Volt_Low, Relay control, toggle switch state }
uint16_t modbus_array[] = {0, 0, 0, 0, 0 ,0};
uint16_t volt, current;
bool intrelock_state=0;
bool rf_state=0;
bool res_state=0;
bool fan12_state=0;
bool fan34_state=0;

void setup() {
  // Init Pin modes
  analogReference(EXTERNAL);
  pinMode(TX_EN_PIN, OUTPUT);
  pinMode(BUZZER_PIN, OUTPUT);
  pinMode(RF_RELAY_PIN, OUTPUT); 
  pinMode(RES_RELAY_PIN, OUTPUT); 
  pinMode(FAN1_RELAY_PIN, OUTPUT); 
  //pinMode(FAN2_RELAY_PIN, OUTPUT); 
  pinMode(FAN3_RELAY_PIN, OUTPUT); 
  //pinMode(FAN4_RELAY_PIN, OUTPUT);  
  pinMode(FAN5_RELAY_PIN, OUTPUT);
  pinMode(MOD_ID0_PIN, INPUT); 
  pinMode(MOD_ID1_PIN, INPUT);
  pinMode(MOD_ID2_PIN, INPUT);
  pinMode(MOD_ID3_PIN, INPUT);
  pinMode(Micro_SW_PIN, INPUT);
  
  // Initial Outputs values
  digitalWrite(TX_EN_PIN, LOW);
  digitalWrite(BUZZER_PIN, LOW);
  digitalWrite(RF_RELAY_PIN, LOW);
  digitalWrite(RES_RELAY_PIN, LOW);
  digitalWrite(FAN1_RELAY_PIN, LOW);
  //digitalWrite(FAN2_RELAY_PIN, LOW);
  digitalWrite(FAN3_RELAY_PIN, LOW);
  //digitalWrite(FAN4_RELAY_PIN, LOW);
  digitalWrite(FAN5_RELAY_PIN, LOW);
  
  // Initiate a MODBUS object
  modbus_port = Modbus(MOD_BUS_ID, 0, TX_EN_PIN);

  // Start the serial port with a specified baud rate
  modbus_port.begin(MOD_BUS_BAUD);
  //modbus_port.start();
  
  //Start Beep
  beep(1,100,100);
}

int micro_SW_State;

void loop() {
  // Allow the master to access the registry table
  modbus_port.poll(modbus_array, sizeof(modbus_array)/sizeof(modbus_array[0]));

  if (bitRead(modbus_array[0],0) == 0) {
    digitalWrite(RF_RELAY_PIN, LOW);
    if(rf_state==1)
    {
      //Beep Error -1
      beep(1,100,100);
    }
    rf_state = 0;
  } else {
    digitalWrite(RF_RELAY_PIN, HIGH);
    if(rf_state==0)
    {
      //Beep Error -1
      beep(2,100,100);
    }
    rf_state = 1;
  }
  if (bitRead(modbus_array[0],1) == 0) {
    digitalWrite(RES_RELAY_PIN, LOW);
    if(res_state==1)
    {
      //Beep Error -1
      beep(1,100,100);
    }
    res_state = 0;
  } else {
    digitalWrite(RES_RELAY_PIN, HIGH);
    if(res_state==0)
    {
      //Beep Error -1
      beep(2,100,100);
    }
    res_state = 1;
  }
  if (bitRead(modbus_array[0],2) == 0) {
    digitalWrite(FAN1_RELAY_PIN, LOW);
    if(fan12_state==1)
    {
      //Beep Error -1
      beep(1,100,100);
    }
    fan12_state = 0;
  } else {
    digitalWrite(FAN1_RELAY_PIN, HIGH);
    if(fan12_state==0)
    {
      //Beep Error -1
      beep(2,100,100);
    }
    fan12_state = 1;
  }
  if (bitRead(modbus_array[0],3) == 0) {
    digitalWrite(FAN3_RELAY_PIN, LOW);
    if(fan34_state==1)
    {
      //Beep Error -1
      beep(1,100,100);
    }
    fan34_state = 0;
  } else {
    digitalWrite(FAN3_RELAY_PIN, HIGH);
    if(fan34_state==0)
    {
      //Beep Error -1
      beep(2,100,100);
    }
    fan34_state = 1;
  }
  if (bitRead(modbus_array[0],4) == 0) {
    digitalWrite(FAN5_RELAY_PIN, LOW);
  } else {
    digitalWrite(FAN5_RELAY_PIN, HIGH);
  }

  //Read MicroSW
  micro_SW_State = analogRead(Micro_SW_PIN);
  if (micro_SW_State < 100) {
    bitWrite(modbus_array[1], 0, 1);
    if(intrelock_state==0)
    {
      //Beep Error -1
      beep(1,100,100);
    }
    intrelock_state = 1;
  } else {
    bitWrite(modbus_array[1], 0, 0);
    if(intrelock_state==1)
    {
      //Beep Error -3
      beep(3,100,100);
    }
    intrelock_state = 0;
  }  
  //Read A/D Volt and Current
  volt= analogRead( VOLT_PIN ); 
  current= analogRead( Current_PIN ); 
  modbus_array[2]= volt; //& 0xff;
  modbus_array[3]= current; //(volt >> 8);
  //modbus_array[4]= current & 0xff;
  //modbus_array[5]= (current >> 8);
  
}

void beep(int num_of_beeps, int beeps_duration_ms, int beeps_inerval_ms){
  int i;
  for(i=0; i<num_of_beeps; i++)
  {
    digitalWrite(BUZZER_PIN, HIGH);
    delay(beeps_duration_ms);
    digitalWrite(BUZZER_PIN, LOW);
    delay(beeps_inerval_ms);
  } 
}
